FROM fredboat/lavalink:dev

ENTRYPOINT ["java", "-Djdk.tls.client.protocols=TLSv1.1,TLSv1.2", "-Xmx256m", "-jar", "Lavalink.jar"]